<?php

require_once './inc/query.php';
require_once './inc/token.php';
require_once './inc/functions.inc.php';

// Show all errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$data = [
        "query" => $schweeklies_winners_query,
];

$post_data = json_encode($data);

// Create the curl request
$crl = curl_init();
curl_setopt($crl, CURLOPT_URL, "https://api.smash.gg/gql/alpha");
curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($crl, CURLOPT_POST, true);
curl_setopt(
        $crl,
        CURLOPT_HTTPHEADER,
        array(
                'Content-Type: application/json',
                "Authorization: Bearer $token"
        )
);
curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

// Get the curl result
$result = curl_exec($crl);

// Read the json
$json_result = json_decode($result, true);

// Get the tournaments
$tournaments = $json_result["data"]["tournaments"]["nodes"];

$winners = array();

// Add the winners in the array
foreach ($tournaments as $tournament) {
        $event = $tournament["events"][0];
        $standings = $event["standings"]["nodes"];

        // Check if there are standings
        if (count($standings) == 0) {
                continue;
        }

        $entrant = $standings[0]["entrant"];
        $player = $entrant["participants"][0]["player"];

        $id = $player["id"];
        $gamer_tag = $player["gamerTag"];

        if (array_key_exists($id, $winners)) {
                $winners[$id]["wins"] += 1;
        } else {
                $winners[$id] = [
                        "gamer_tag" => $gamer_tag,
                        "wins" => 1,
                ];
        }
}

// Sort the array to get the one with the most wins first
usort($winners, "compare_winners");

// Get the different wins
$wins = array();
foreach ($winners as $winner) {
        if (!in_array($winner["wins"], $wins)) {
                array_push($wins, $winner["wins"]);
        }
}

rsort($wins);

// Print the winners
foreach ($winners as $key => $winner) {
        $rank = array_search($winner["wins"], $wins) + 1;
        $rankText = "";
        if ($rank == 1) {
                $rankText = "# 1st";
        } else if ($rank == 2) {
                $rankText = "## 2nd";
        } else if ($rank == 3) {
                $rankText = "### 3rd";
        } else {
                $rankText = $rank . "th";
        }


        $key += 1;
        $win_text = $winner["wins"] > 1 ? "victories" : "victory";
        echo "<p>";
        if ($rank <= 3) {
                echo "$rankText " . $winner["gamer_tag"] . " (" . $winner["wins"] . " $win_text" . ")" . PHP_EOL;
        } else {
                echo "**$rankText " . $winner["gamer_tag"] . " (" . $winner["wins"] . " $win_text" . ")**" . PHP_EOL;
        }

        echo "</p>";
}

// Close cURL session handle
curl_close($crl);

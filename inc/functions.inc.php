<?php
function compare_winners($a, $b)
{
	if ($a["wins"] == $b["wins"]) {
		return 0;
	}

	return ($a["wins"] > $b["wins"]) ? -1 : 1;
}

<?php

$schweeklies_winners_query = 'query SchweekliesWinners{
    tournaments(query: {
      #perPage: $perPage
      filter: {
        name: "Schweeklies"
      } 
    }) {
    nodes {
      name
      events {
      standings(query: {
      	perPage: 1,
        sortBy: "placement"
    	}){
      	nodes {
        	entrant {
            participants{
              player{
                id
                gamerTag
              }
            }
        	}
      	}
    	}
    }
    }
  }
}';

# Top Schweeklies

PHP script to get the leaderboard of all Sckweeklies.

## Get the leaderboard

You can visit http://schweeklies.stowy.ch

## Set up the project

### Token

After clonning, you should create a file called `token.php` in `inc/` that looks like this :

```PHP
<?php
$token = 'YOUR_TOKEN_HERE';
```

### cURL

This project uses curl, so you need to activate it in your `php.ini`.
On debian it is located at : `/etc/php/7.3/apache2/php.ini`.

You also need to install it via apt :

```bash
sudo apt install php7.3-curl
```
